﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeataProject.model
{

    /// <summary>
    /// undo_log 撤销日志表
    /// </summary>
    public class undo_log
    {
        public int Id { get; set; }

        public int branch_id { get; set; }

        public string xid { get; set; }

        public string context { get; set; }

        public string rollback_info { get; set; }

        public int log_status { get; set; }

        public DateTime log_created { get; set; }

        public DateTime log_modified { get; set; }

        public string ext { get; set; }
    }
}

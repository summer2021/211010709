﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeataProject.model
{

    public class rollBackInfo 
    {
        public string tableName { get; set; }

        public string  primaryKey { get; set; }

        public string primaryValue { get; set; }

        public string updateKey { get; set; }

        public string updateBeforeValue { get; set; }

        public string updateAfterValue { get; set; }


    }

}

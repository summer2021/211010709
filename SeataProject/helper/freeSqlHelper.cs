﻿using FreeSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeataProject.helper
{
    public class DBContext : DbContext
    {
        public string connectStr;
        public DBContext(string connectStr)
        {
            
        }
        public IFreeSql fsql { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            //Data Source = 127.0.0.1; Port = 3306; User ID = root; Password = root; Initial Catalog = cccddd; Charset = utf8; SslMode = none; Min pool size = 1
            //var connectStr = "Data Source=123.57.194.245;Initial Catalog=seataTest1;User ID =root; Password=junbaomysql123456; allowPublicKeyRetrieval=true; pooling=true; CharSet=utf8;port=3307; sslmode=none; AllowLoadLocalInfile=true";

           var connectStr = "Data Source=123.57.194.245;Port=3307;User ID=root;Password=junbaomysql123456; Initial Catalog=seataTest1;Charset=utf8; SslMode=none;Min pool size=1";
            FreeSql.DataType dataType = FreeSql.DataType.MySql;
            fsql = new FreeSql.FreeSqlBuilder()
        .UseConnectionString(dataType,connectStr)
        .UseAutoSyncStructure(true) //自动同步实体结构到数据库
        .UseLazyLoading(true)
        .Build(); //请务必定义成 
            builder.UseFreeSql(fsql);
        }


        protected override void OnModelCreating(ICodeFirst codefirst)
        {

        }
    }
}

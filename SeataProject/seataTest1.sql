/*
 Navicat Premium Data Transfer

 Source Server         : ECSMySql
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : 123.57.194.245:3307
 Source Schema         : seataTest1

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 14/08/2021 20:19:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for undo_log
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log`  (
  `id` int(0) NOT NULL,
  `branch_id` int(0) NOT NULL,
  `xid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `context` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rollback_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `log_status` int(0) NOT NULL,
  `log_created` datetime(3) NOT NULL,
  `log_modified` datetime(3) NOT NULL,
  `ext` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `ux_undo_log`(`xid`, `branch_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of undo_log
-- ----------------------------
INSERT INTO `undo_log` VALUES (0, 4302188, '4302188', '{\"tableName\":null,\"primaryKey\":\"id\",\"primaryValue\":\"1\",\"updateKey\":\"money\",\"updateBeforeValue\":\"1\",\"updateAfterValue\":\"1\"}', '{\"tableName\":null,\"primaryKey\":\"id\",\"primaryValue\":\"1\",\"updateKey\":\"money\",\"updateBeforeValue\":\"1\",\"updateAfterValue\":\"1\"}', 1, '2021-08-14 20:11:18.436', '2021-08-14 20:11:18.436', '');

-- ----------------------------
-- Table structure for userAccount
-- ----------------------------
DROP TABLE IF EXISTS `userAccount`;
CREATE TABLE `userAccount`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `money` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  `bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '银行',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of userAccount
-- ----------------------------
INSERT INTO `userAccount` VALUES (1, 'test1', '1', '测试', '2021-08-14 08:30:56', '2021-08-14 08:30:58', '建设银行');
INSERT INTO `userAccount` VALUES (2, 'test2', NULL, NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;

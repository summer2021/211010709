﻿using SeataProject.taManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeataProject
{
    class Program
    {
        static void Main(string[] args)
        {
            //Seata是阿里开发的一个用于微服务架构的高性能易使用的分布式事务框架

            //默认事务模式AT

            //下阶段主要目标 学习事务模式TCC 的原理、数据库设计、通用类库编写、测试

            //AT 2PC 协议 

            //TC： Transaction Coordinator，事务协调器：监视每个全局事务的状态，负责全局事务的提交和回滚。

            //AT 模式SDK 实现

            // 2个数据库 提交回滚
            //Mysql AT 模式
            //当前阶段完成 2个数据库 回滚Demo 
            //下一阶段封装SDK 进行实现
            //简单粗暴尝试需要一个内存记录，如果成功！ 则提交 如果失败则 删除之前已经成功的！

            var connectStr1 = @"Data Source = 123.57.194.245; Database = seataTest1; User ID = root; Password = junbaomysql123456; allowPublicKeyRetrieval = true; pooling = true; CharSet = utf8; port = 3307; sslmode = none; AllowLoadLocalInfile = true"; ;
            var connectStr2 = @"Data Source = 123.57.194.245; Database = seataTest2; User ID = root; Password = junbaomysql123456; allowPublicKeyRetrieval = true; pooling = true; CharSet = utf8; port = 3307; sslmode = none; AllowLoadLocalInfile = true"; ; ;
            var taManager = new TAManager(connectStr1, connectStr2);
            var sql1 = "update userAccount set money=1 where id=1";
            var sql2 = "update userAccount set money=1 where id=2";
            taManager.distributedTransaction(sql1, sql2);

          
        }
    }
}

﻿using Newtonsoft.Json;
using SeataProject.helper;
using SeataProject.model;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft;

namespace SeataProject.taManager
{
    public class TAManager
    {

        DBContext context1 = null;
        DBContext context2 = null;
        public TAManager(string connectStr1,string connectStr2)
        {
            context1 = new DBContext(connectStr1);
            context2 = new DBContext(connectStr2);
        }

        public string getTableName(string sql)
        {
            //sql1 = "update userAccount set money=1 where id=1";
            var setIndex = sql.IndexOf("set");
            var tableName = sql.Substring(7, setIndex-7);
            return tableName;
        }
        public string getUpdateKey(string sql)
        {
            //sql1 = "update userAccount set money=1 where id=1";

            var setIndex = sql.IndexOf("set");
            var whereIndex = sql.IndexOf("where");

            var updateKey = sql.Substring(setIndex + 3, whereIndex - (setIndex + 3)).Trim().Split('=')[0];
            return updateKey;
        }
        public string getUpdateValue(string sql)
        {
            //sql1 = "update userAccount set money=1 where id=1";

            var setIndex = sql.IndexOf("set");
            var whereIndex = sql.IndexOf("where");

            var updateKey = sql.Substring(setIndex + 3, whereIndex - (setIndex + 3)).Trim().Split('=')[1];
            return updateKey;
        }
        public string getPrimaryKey(string sql)
        {
            //sql1 = "update userAccount set money=1 where id=1";

            var whereIndex= sql.IndexOf("where");
            var primaryKey = sql.Substring(whereIndex + 5, sql.Length - whereIndex - 5).Trim().Split('=')[0];
            return primaryKey;
        }
        public string getPrimaryValue(string sql)
        {
            //sql1 = "update userAccount set money=1 where id=1";

            var whereIndex = sql.IndexOf("where");
            var primaryKey = sql.Substring(whereIndex + 5, sql.Length - whereIndex -5).Trim().Split('=')[1];
            return primaryKey;
        }

        public string getUpdateBeforeSql(string updateKey,string primaryKey,string primaryValue,string tableName)
        {
            var sql1 = $"select {updateKey} from {tableName} where {primaryKey}={primaryValue}";
            return sql1;
        }

        public string getRollBackSql(string updateKey,string updateValue, string primaryKey, string primaryValue, string tableName)
        {
            var sql = $"update {tableName} set {updateKey}={updateValue} where {primaryKey}={primaryValue}";
            return sql;
        }

        public void generateUndoLog(string sql)
        {
            var branchId = new Random().Next(10000, 10000000);
            var tableName = getTableName(sql1);
            var primaryKey = getPrimaryKey(sql1);
            var primaryValue = getPrimaryValue(sql1);
            var updateKey = getUpdateKey(sql1);
            var updateAfterValue = getUpdateValue(sql1);
            var updateBeforeSql = getUpdateBeforeSql(updateKey, primaryKey, primaryValue, tableName);
            var updateBeforeValue = context1.fsql.Ado.ExecuteScalar(updateBeforeSql);

            rollBackInfo rollBack = new rollBackInfo();
            rollBack.primaryKey = primaryKey;
            rollBack.primaryValue = primaryValue;
            rollBack.updateKey = updateKey;
            rollBack.updateBeforeValue = updateBeforeValue.ToString();
            rollBack.updateAfterValue = updateAfterValue;

            undo_log log = new undo_log();
            log.branch_id = branchId;
            log.context = JsonConvert.SerializeObject(rollBack);
            log.rollback_info = JsonConvert.SerializeObject(rollBack);
            log.log_created = DateTime.Now;
            log.log_modified = DateTime.Now;
            log.log_status = 1;
            log.ext = string.Empty;
            log.xid = branchId.ToString();

            context1.Add<undo_log>(log);
            context1.SaveChanges();

        }
        /// <summary>
        /// 执行分布式事务
        /// </summary>
        /// <param name="sql1"></param>
        /// <param name="sql2"></param>
        /// <returns></returns>
        public bool distributedTransaction(string sql1,string sql2)
        {

            var branchId = new Random().Next(10000, 10000000);
            var tableName = getTableName(sql1);
            var primaryKey = getPrimaryKey(sql1);
            var primaryValue = getPrimaryValue(sql1);
            var updateKey = getUpdateKey(sql1);
            var updateAfterValue = getUpdateValue(sql1);
            var updateBeforeSql = getUpdateBeforeSql(updateKey, primaryKey, primaryValue, tableName);
            var updateBeforeValue = context1.fsql.Ado.ExecuteScalar(updateBeforeSql);

            rollBackInfo rollBack = new rollBackInfo();
            rollBack.primaryKey = primaryKey;
            rollBack.primaryValue = primaryValue;
            rollBack.updateKey = updateKey;
            rollBack.updateBeforeValue = updateBeforeValue.ToString();
            rollBack.updateAfterValue = updateAfterValue;

            undo_log log = new undo_log();
            log.branch_id = branchId;
            log.context= JsonConvert.SerializeObject(rollBack);
            log.rollback_info = JsonConvert.SerializeObject(rollBack);
            log.log_created = DateTime.Now;
            log.log_modified = DateTime.Now;
            log.log_status = 1;
            log.ext = string.Empty;
            log.xid = branchId.ToString();

            context1.Add<undo_log>(log);
            context1.SaveChanges();


            //其实我只需要记录更新之前的数据就可以了
            //update `userAccount` set money=1 where id=1
            //update `userAccount` set money=1 where id=1

            //中国银行 用户1 转账 扣钱
            //农业银行 用户2 转账 余额加钱
            //中国银行和农业银行在两个数据库，使用seata 分布式事务 AT 模式
            //如果用户1的余额 大于1 ，则事务成功

            //如果用户1的余额小于 1，则事务失败，回滚

            //解析生成sql 获取表名称 字段名称 逐渐 名称
            sql1 = "update userAccount set money=1 where id=1";


           

            return true;
        }
    }
}
